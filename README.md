# kTemplates
### Minimalist PHP Template engine

## Barely 800 lines of code and full of stuff
Little things are easier to handle, I always keep it in mind.

- Built on regular expressions.
- Support for MySQL and CSV data sources.
- Conditionals for PHP variables and loops for SQL queries.
- Caching (memcache in progress...)
- Modifiers, with easy way of expansion.
- Includes inside templates.
- Callbacks for each row.
- Really simple plugins system (in progress...)
- All standard PHP, no nasty libraries.

[Source code](https://github.com/kashopi/ktemplates) available on Github.

## More about the project
One day [Nacho "Kashopi" Rodríguez] had to deal with Smarty for PHP. The tool, though great, was too heavy for the purposes he had to face so he created [kTemplates]. Many websites are using this little engine obtaining great advantages on performance and simplicity of use.

- [Author's webpage] (http://freecrats.com)

## Credits
Created by [Nacho Rodríguez](http://twitter.com/kashopi).

Copyright (c) 2006 by Nacho Rodríguez.