<?php
/*
Autor: Nacho Rodriguez (jignacio.svq@gmail.com)
Licencia GPL (http://www.gnu.org/copyleft/gpl.html)
*/
class Template{
	private $_theme;
	private $_tfile;
	private $_tcontent;
	private $_result;
	private $_isplugin;
	private $_cacheparams;
	private $_cache_valida;
	private $_generar_cache;
	private $_cache_directory_storing;
	private $_cache_TTL;
	private $_snippets;
	private $_enableLog;
	private $_logfp;
	public  $_maxiteraciones;
	public  $_niteraciones;
	private $_linkDB;
	private $_alinkDB;

	function init(){
		$this->_theme='';
		$this->_tfile='';
		$this->_tcontent='';
		$this->_result='';
		$this->_isplugin=false;
		$this->_plugin='';
		$this->_cacheparams='';
		$this->_cache_valida=false;
		$this->_generar_cache=false;
		$this->_cache_directory_storing='';
		$this->_cache_TTL=60;
		$this->_maxiteraciones=10000;
		$this->_niteraciones=0;
		$this->_enableLog=false;
		$this->_snippets=array();
		$this->_alinkDB=array();
		unset($this->_linkDB);
	}

	function addDBConnection($link,$nombre='DEFAULT'){
		
		$this->logMessage("addDBConnection $link,$nombre");
		
		if(sizeof($this->_alinkDB)==0)
			$this->_linkDB=$link;
			
		$this->_alinkDB[$nombre]=$link;
	}
	
	function end(){
		$this->_theme='';
		$this->_tfile='';
		$this->_tcontent='';
		$this->_result='';
		$this->_isplugin=false;
		$this->_plugin='';
		$this->_cacheparams='';
		$this->_cache_valida=false;
		$this->_generar_cache=false;
		$this->_cache_directory_storing='';
		$this->_maxiteraciones=10000;
		$this->_snippets=array();
		unset($this->_linkDB);
	}

	function setPlugin($nombre){
		$this->_isplugin=true;
		$this->_plugin=$nombre;
	}

	function setTheme($tema){
		$this->_theme=$tema;
		$this->logMessage("setTheme $tema");
	}

	function getTheme(){
		return($this->_theme);
	}

	function loadTemplate($plantilla){
		$this->logMessage("loadTemplate $plantilla");
		$this->_tfile=$plantilla;
		$this->_tcontent=file_get_contents($this->getTPath($plantilla));
		$this->get_cache_parameters();
		$this->include_needed_templates();
		$this->include_configuration();
	}

	function enableCache(){
		$this->logMessage("enableCache: true");
		$this->_generar_cache=true;
	}

	function procesaSustitucion($c,$v,&$template){
		if(is_array($v)){ $array=$v; $nombre=$c;}
		else{ $array=array($c=>$v); $nombre='';}
		
		foreach($array as $indice=>$valor){
			//Simples
			if($nombre!='') $campo=$nombre.'['.$indice.']';
			else $campo=$indice;
			
			$template=str_replace("{". $campo ."}",$valor,$template);
			
			//Complejas
			if($nombre!=''){ $campo_reg=$nombre.'\['.$indice.'\]'; $campo=$nombre.'['.$indice.']'; }
			else { $campo_reg=$indice; $campo=$indice; }
			while(preg_match("/\{".$campo_reg.":([^}]+)\}/",$template,$res))
				$template=str_replace("{". $campo .":" . $res[1] . "}",$this->procesaModificadores($valor,$res[1]),$template);
			
		}
	}

	function procesaModificadores($valor,$modificadores){
		$modificadores_array=explode(";",$modificadores);
		foreach($modificadores_array as $modificador){
			$tokens=explode("=",$modificador);
			$accion=$tokens[0];
			$parametro=$tokens[1];
			switch($accion){
				case 'longitud':
				case 'length':
					$l=strlen($valor);
					$valor=substr($valor,0,$parametro);
					if ($l!=strlen($valor))
					$valor .= '...';
					break;
				case 'mayusculas':
				case 'uppercase':
					$valor=strtoupper($valor);
					break;
				case 'minusculas':
				case 'lowercase':
					$valor=strtolower($valor);
					break;
				case 'wordwrap':
					$valor=wordwrap($valor, $parametro, "<br/>\n");
					break;
				case 'sinespacios':
				case 'stripspaces':
					$valor=str_replace(' ',$parametro,$valor);
					break;
				case 'html':
					$valor=htmlspecialchars($valor);
					break;
				case 'fecha':
				case 'date':
					if($valor!=''){
						if($parametro!=''){
							$dia=substr($valor,8,2);
							$mes=substr($valor,5,2);
							$year=substr($valor,0,4);
							$valor=mktime(0,0,0,$mes,$dia,$year);
							$valor=date($parametro,$valor);
							if($accion=='fecha'){
								$valor=str_replace('January','Enero',$valor);
								$valor=str_replace('February','Febrero',$valor);
								$valor=str_replace('March','Marzo',$valor);
								$valor=str_replace('April','Abril',$valor);
								$valor=str_replace('May','Mayo',$valor);
								$valor=str_replace('June','Junio',$valor);
								$valor=str_replace('July','Julio',$valor);
								$valor=str_replace('August','Agosto',$valor);
								$valor=str_replace('September','Septiembre',$valor);
								$valor=str_replace('October','Octubre',$valor);
								$valor=str_replace('November','Noviembre',$valor);
								$valor=str_replace('December','Diciembre',$valor);
							}
						}
					}
					break;
				case 'limpio':
				case 'cleanup':
					$valor=$this->convierte_caracteres_malos($valor);
					break;
				case 'solosiexiste':
				case 'onlyif':
					eval("global \$$parametro;");
					eval("\$v=\$$parametro;");
					if($v==''){
						$valor="";
						break 2;
					}
					break;
				case 'addslash':
					$valor .= "/";
					break;
				case 'moneda':
				case 'money':
					$valor=number_format($valor,2,",",".");
					if($parametro=='&euro') $parametro='&euro;';
					$valor .= " $parametro";
					break;
				case 'enum':
					$parametros=explode(":",$parametro);
					settype($valor,"int");
					$valor=$parametros[$valor];
					break;
				case 'idnencode':
					$IDNlocal = new idna_convert();
					$valor = $IDNlocal->encode($valor);
					break;
				case 'idndecode':
					$IDNlocal = new idna_convert();
					$valor = $IDNlocal->decode($valor);
					break;
				case 'utf8encode':
					$valor = utf8_encode($valor);
					break;
				case 'utf8decode':
					$valor = utf8_decode($valor);
					break;
				case 'math_operation':
					$parametro=str_replace(';','',$parametro);
					eval("\$valor=\$valor$parametro;");
					break;
				case 'ifnull':
					$parametros=explode(':',$parametro);
					if($valor=='NULL')
						$valor='<font color="'.$parametros[1].'">'.$parametros[0].'</font>';
					break;
				case 'nl2br':
					$valor=nl2br($valor);
					break;
				case 'eliminar':
				case 'remove':
					$parametros=explode(':',$parametro);

					foreach($parametros as $parametro){
						$valor=str_replace($parametro,'',$valor);
					}
			}
		}
		return $valor;
	}

	function parseTemplate($sustituciones){
		foreach($sustituciones as $campo=>$valor)
			$this->procesaSustitucion($campo,$valor,$this->_tcontent);
		
	}

	function parseConfigurationVars(){
		while(preg_match("/{#([a-zA-Z0-9_]+)#}/",$this->_tcontent,$res)){
			eval($valor . "=$_CONF_" . $res[1] . ";");
			$this->_tcontent=str_replace("#".$res[1]."#}",$valor,$this->_tcontent);
		}
	}

	function parseIterations(){
		$iteraciones=0;
		$this->logMessage("ParseIterations: start");
		while(ereg("{([a-zA-Z0-9_-]+):([a-zA-Z0-9]+:){0,1}foreach ([a-zA-Z0-9_#;]+):([a-zA-Z0-9:=;_/]+)}(.+){([a-zA-Z0-9_-]+):end}",$this->_tcontent,$res) && $iteraciones<=20){
			$bloque=$res[1];
			$conexion=$res[2];
			$this->logMessage(" ..... bloque: $bloque");
			$this->logMessage(" ..... conexion: $conexion");
			if($conexion!='') { $this->_linkDB=$this->_alinkDB[substr($conexion,0,-1)]; }
			ereg("{".$bloque.":$conexion"."foreach ([a-zA-Z0-9_#;]+):([a-zA-Z0-9:=;_/]+)}(.+){".$bloque.":end}",$this->_tcontent,$res);
			$tipo_iteracion=substr($res[1],0,3);
			switch($tipo_iteracion){
				case "sql":
					$p=$this->parseSQL('mysql',$res[1],$res[2],$res[3]);
					break;
				case "slt":
					$p=$this->parseSQL('sqlite',$res[1],$res[2],$res[3]);
					break;
				case "csv":
					$p=$this->parseCSV($res[1],$res[2],$res[3]);
					break;
				default:
					$p=$this->parseSQL($res[1],$res[2],$res[3]);
			}
			
			$this->_tcontent=ereg_replace("{".$bloque.":$conexion"."foreach ([a-zA-Z0-9_#;]+):([a-zA-Z0-9:=;_/]+)}(.+){".$bloque.":end}",$p,$this->_tcontent);
			$iteraciones++;
		}
		if($iteraciones>=20) echo "ITERATIONS ERROR: MAXIMUM ITEMS REACHED.";
		$this->logMessage("ParseIterations: end");
	}

	function parseSnippets(){
		while(ereg("{([a-zA-Z0-9_-]+):snippet}(.+){([a-zA-Z0-9_\-\']+):end}",$this->_tcontent,$res)){
			$bloque=$res[1];
			$this->_snippets[$bloque]=$res[2];
			$this->_tcontent=ereg_replace("{".$bloque.":snippet}(.+){".$bloque.":end}","",$this->_tcontent);
		}
	}

	function parseConditionals(){
		$this->logMessage("ParseConditionals: start");
		while(preg_match("/\{([a-zA-Z0-9_-]+):if ([a-zA-Z0-9_]+)\(([\$a-zA-Z0-9_:]+)\)(.{2})([a-zA-Z0-9:=;_]+)\}/",$this->_tcontent,$res)){
			$bloque=$res[1];
			$scope=$res[2];
			$variable=$res[3];
			$condicion=$res[4];
			$valor=$res[5];
			
			$this->logMessage("..... bloque: $bloque");
			$this->logMessage("..... eval: $variable $condicion $valor");
			
			$cachoU='{'.$bloque.':if '.$scope.'('.$variable.')'.$condicion.''.$valor.'}';
			$cachoD='{'.$bloque.':end}';
			$i=strpos($this->_tcontent,$cachoU);
			$f=strpos($this->_tcontent,$cachoD);
			
			$trozo=substr($this->_tcontent,$i+strlen($cachoU),$f-$i-strlen($cachoU));
			$trozo_arriba=substr($this->_tcontent,0,$i);
			$trozo_abajo=substr($this->_tcontent,$f+strlen($cachoD));

			$p=$this->parseIF($scope,$variable,$condicion,$valor,$trozo);
			$this->_tcontent=$trozo_arriba.$p.$trozo_abajo;
		}
		$this->logMessage("ParseConditionals: end");
	}

	function parseIF($scope,$variable,$condicion,$valor,$plantilla){
		$r=false;
		switch($scope){
			case "system":
				eval("global \$".$variable .";");
				if($valor=="_VACIO_") $valor="''";
				eval("\$r=(\$".$variable.$condicion.$valor.");");
				break;
			case "config":
				break;
			default:
				break;
		}
		if($r)
		return($plantilla);
		else
		return("");

	}
	
	function parseSQL($type,$sql,$sustituciones,$plantilla){
		$total="";
		$using_subtotales=false;
		$contador_subtotales=0;
		eval("global \$".$sql .";");
		eval("\$sql=\$".$sql .";");

		$total_money=0;
		if($sql!="")
		{
			if($type=='mysql') $res=mysql_query($sql,$this->_linkDB);
			if($type=='sqlite') $res=$this->_linkDB->query($sql);

			$s=explode(";",$sustituciones);
			$ir=0;
			while ($fila=mysql_fetch_array($res)) { //// CONTINUE HERE
				$ir++;
				$trozo=$plantilla;
				foreach($s as $campos){
					$campos=explode("=",$campos);
					
					switch($campos[0]){
						case "_ALT_FILAS":
							$estilo=explode("/",$campos[1]);
							$this->procesaSustitucion($campos[0],$estilo[$ir % 2],$trozo);
							break;
						case "_SELECT":
							$params=explode("/",$campos[1]);
							$campoBD=$params[0];
							$valorBD=$params[1];
							
							if($fila[$campoBD]==$valorBD)
								$selected="selected=\"selected\"";
							else
								$selected="";
								
							$this->procesaSustitucion('_SELECT',$selected,$trozo);
							
							break;
						default:
							$this->procesaSustitucion($campos[0],$fila[$campos[1]],$trozo);
					}

				}
				$contador_subtotales++;
				$trozo=$this->procesaCallbacks($trozo,$fila);
				$total .= $trozo;
			}
		}

		if($using_subtotales){
			$trozo_subtotal=$this->_snippets[$subtotales_params[2]];
			$this->procesaSustitucion($subtotales_params[1],$total_money,$trozo_subtotal);
			$total .= $trozo_subtotal;
		}
		
		$this->_niteraciones=$ir;
	
		if($this->_maxiteraciones<$ir){
			global $MAX_FILAS;
			$MAX_FILAS=true;
		}

		return($total);
	}

	function parseCSV($variable,$sustituciones,$plantilla){
		$total="";
		eval("global \$".$variable .";");
		eval("\$local_array=\$".$variable .";");
		$s=explode(";",$sustituciones);
		$ir=0;
		foreach($local_array as $linea_datos) {
			$orden_campos=0;
			$ir++;
			$trozo=$plantilla;
			$linea_datos=explode("|",$linea_datos);
			foreach($s as $campos){
				$campos=explode("=",$campos);
				switch($campos[0]){
					case "_ALT_FILAS":
						$estilo=explode("/",$campos[1]);
						$this->procesaSustitucion($campos[0],$estilo[$ir % 2],$trozo);
						break;
					case "_SELECT":
						$valores_select=explode("/",$campos[1]);
						if($valores_select[1]==$res[$valores_select[0]])
							$this->procesaSustitucion($campos[0],"SELECTED",$trozo);
						else
							$this->procesaSustitucion($campos[0],"",$trozo);
						break;
					default:
						$this->procesaSustitucion($campos[0],$linea_datos[$orden_campos],$trozo);
				}
				
				$orden_campos++;
			}
			$trozo=$this->procesaCallbacks($trozo,$linea_datos[0]);
			$total .= $trozo;
		}

		$this->_niteraciones=$ir;
			
		if($this->_maxiteraciones<$ir){
			global $MAX_FILAS;
			$MAX_FILAS=true;
		}
	
		return($total);
	}
	
	function procesaCallbacks($plantilla,$variables){
		while(ereg("{callback\(([a-zA-Z0-9_]+)[:0-9]*\)}",$plantilla,$res)){
			$funcion_cb="callback_" . $res[1] . "_get";
			$this->logMessage("procesaCallbacks: $funcion_cb");
			$trozo=$funcion_cb($plantilla,$variables);
			$plantilla=str_replace($res[0],$trozo,$plantilla);
		}
		return($plantilla);
	}

	function parsePlugins(){
		global $__PLUGINS_DIR;
		while(ereg("{plugin:([a-zA-Z0-9_\-\.]+)\(([a-zA-Z0-9_\-\:]+)\)}",$this->_tcontent,$res)){
			include_once($__PLUGINS_DIR . "/" . $res[1] . ".php");
			eval("\$trozo=plugin_" . $res[1] . "_get(\"" . $res[2] . "\");");
			$this->_tcontent=str_replace($res[0],$trozo,$this->_tcontent);
		}
	}

	function parseImages(){
		while(ereg("{image:([a-zA-Z0-9_\-\.]+):([^\}]+)}",$this->_tcontent,$res)){
			$trozo="<img src=\"/" . $this->getTPath("img/" . $res[1],false) . "\" " . $res[2] . ">";
			$this->_tcontent=str_replace($res[0],$trozo,$this->_tcontent);
		}
	}

	function parseCSS(){
		while(ereg("{style:([a-zA-Z0-9_\-\.]+)}",$this->_tcontent,$res)){
			$trozo="<link rel=\"stylesheet\" type=\"text/css\" href=\"/" . $this->getTPath("style/" . $res[1],false) . "\">";
			$this->_tcontent=str_replace($res[0],$trozo,$this->_tcontent);
		}
	}

	function get($sustituciones=array()){
		global $__USE_CACHED_CONTENTS;
		
		$this->logMessage("get: start");
		if (!$this->cacheValida()){
			$this->parseSnippets();
			$this->parseConditionals();
			$this->parseIterations();
			$this->parseConditionals();

			do{
				$this->logMessage("### get iteration ###");
				$this->parseTemplate($sustituciones);
			}while($this->include_needed_templates());
			
			$this->parseConditionals();

			$this->parseIterations();
			
			$this->_tcontent=$this->procesaCallbacks($this->_tcontent,"");
			$this->parsePlugins();
			$this->parseImages();
			$this->parseCSS();
			if($__USE_CACHED_CONTENTS)
				$this->setCachedContents($this->_tfile,$this->_tcontent);
		}
		
		$this->logMessage("get: end");
		return($this->_tcontent);
	}

	function get_cache_parameters(){
		if (ereg("{CACHE:([^\}]+)}",$this->_tcontent,$res)){
			$params=explode("-",$res[1]);
			foreach ($params as $param){
				eval("global \$$param;");
				eval("\$valor=\$$param;");
				if ($valor!='')
				$this->_cacheparams .= '_' . $valor;
			}
			$this->_tcontent=str_replace("{CACHE:".$res[1]."}","",$this->_tcontent);
		}

		if (ereg("{CACHE-DIRECTORY-STORING:([^\}]+)}",$this->_tcontent,$res)){
			$dirs=explode("-",$res[1]);
			foreach($dirs as $dir){
				eval("global \$$dir;");
				eval("\$valor=\$$dir;");
				$this->_cache_directory_storing .= $valor . "-";
			}
			$this->_tcontent=str_replace("{CACHE-DIRECTORY-STORING:".$res[1]."}","",$this->_tcontent);
		}

		if (ereg("{CACHE-EXPIRES:([0-9]+)}",$this->_tcontent,$res)){
			$this->_cache_TTL=intval($res[1]);
			$this->_tcontent=str_replace("{CACHE-EXPIRES:".$res[1]."}","",$this->_tcontent);
		}
		
	}

	function hasCachedContents($plantilla){
		global $__USE_CACHED_CONTENTS;
		if(!$__USE_CACHED_CONTENTS) return(false);
		$path=$this->getCacheFilename($plantilla);
		if(file_exists($path))
		return(true);
		else
		return(false);
	}

	function getCachedContents($plantilla){
		$path=$this->getCacheFilename($plantilla);
		$this->_tcontent=file_get_contents($path);
		$this->_cache_valida=true;
	}

	function setCachedContents($plantilla,$contenido){
		if($this->_generar_cache){
			$dir=$this->getCacheDirectory();
			if(!is_dir($dir))
				mkdir($dir,0777);

			$path=$this->getCacheFilename($plantilla);
		
			file_put_contents($path,$contenido);
		}
	}

	function cacheValida(){
		if($this->_cache_valida){
			$datefichero=filemtime($this->getCacheFilename($this->_tfile));
			$ahora=getdate();
			$TTL_max=mktime($ahora["hours"],$ahora["minutes"]-$this->_cache_TTL,$ahora["seconds"],$ahora["mon"],$ahora["mday"],$ahora["year"]);
			if($TTL_max>$datefichero)
				return(false);
			else
				return(true);
		}
		else{
			return(false);
		}
	}

	function getCacheFilename($plantilla){
		$path=$this->getTPath("cache",false);
		if($this->_cache_directory_storing)
		    $path .= "/" . $this->_cache_directory_storing;
		$path .= "/" . $plantilla . $this->_cacheparams . ".chtml";
		return($path);
	}

    function getCacheDirectory(){
		$path=$this->getTPath("cache",false);
		$path .= "/";
		if($this->_cache_directory_storing)
		    $path .= $this->_cache_directory_storing . "/";
		return($path);
	}

	function getTPath($plantilla,$extension_auto=true){
		global  $__TEMPLATES_DIR;
		$s=$__TEMPLATES_DIR . "/". $this->_theme;
		if($this->_isplugin)
		$s .= "/plugins/" . $this->_plugin;

		$s .= "/" . $plantilla;
		if($extension_auto) $s .= ".html";
		return($s);
	}

	function include_needed_templates(){
		$this->logMessage("include_needed_templates: start");
		$oInclude=new Template();
		$oInclude->enableCache();
		/*foreach($this->_alinkDB as $nombre=>$conexion){
			$oInclude->addDBConnection($conexion,$nombre);
		}*/
		$oInclude->_maxiteraciones = $this->_maxiteraciones;
		$oInclude->setTheme($this->_theme);
		
		$c_iteracion=0;
		$s=array();
		//$this->logMessage("[TEMPLATE]\n\n".$this->_tcontent."\n\n[----------------]\n");
		while($c_iteracion<$this->_maxiteraciones && ereg("{include:([a-zA-Z0-9]+:){0,1}([a-zA-Z0-9_-]+)(\(([[:graph:]^\)]+)\)){0,1}}",$this->_tcontent,$res)){
			$conexion=$res[1];
			$tiene_parametros=$res[3];
			$this->logMessage(" ...... include: ".$res[2]);
			if($conexion!='') { $token=$conexion; $oInclude->addDBConnection($this->_alinkDB[substr($conexion,0,-1)]); }
			if($tiene_parametros!='') {
					$parametros=$res[4];
					$lparametros=explode(';',$parametros);
					foreach($lparametros as $p){
						$campo=explode('=',$p);
						$s[$campo[0]]=$campo[1];
						$this->logMessage(" ...... vars: ".$campo[0] . "=". $campo[1]);			
					}
			}

			$oInclude->loadTemplate($res[2]);
			$x=$oInclude->get($s);
			$this->_tcontent=str_replace("{include:$token". $res[2] . "$tiene_parametros}",$x,$this->_tcontent);
			$c_iteracion++;
		}
		if($c_iteracion>=$this->_maxiteraciones) echo "ERROR: include , maximo numero de iteraciones alcanzado (".$res[2].")";
		
		$oInclude->end();
		$this->logMessage("include_needed_templates: end");
		
		if($c_iteracion==0) return(false);
		else return(true);
	}

	function include_configuration(){
		while(ereg("{configuration:([a-zA-Z0-9_\-\.]+)}",$this->_tcontent,$res)){
			require($this->getTPath($res[1],false));
			$this->_tcontent=str_replace("{configuration:". $res[1] . "}","",$this->_tcontent);
		}
	}

	function convierte_caracteres_malos($s){
		$s = htmlentities($s);
		$s = preg_replace("/\&(.)[^;]*;/", "\\1", $s);
		$s = str_replace(" ","-",$s);
		return($s);
	}
	
	function enableLog(){
		$this->_enableLog=true;
		@unlink('logs/templates.log');
		$this->_logfp=fopen('logs/templates.log','w');
		$this->logMessage('*** Iniciando registro ***');
	}
	
	function logMessage($msg,$dropStatus=NULL){
		if($this->_enableLog){
			$micro='';
			$micro=microtime();
			$k=explode(' ',$micro);
			$micro=str_replace('0.','',$k[0]);
			$stamp=date('y-m-d H:i:s.') . $micro;
			fputs($this->_logfp,$stamp .":$msg\n");
			
			if($dropStatus!=NULL){
				//file_put_contents('logs/drops/C#'.$stamp.".html",$this->_tcontent);
				//file_put_contents('logs/drops/D#'.$stamp.".html",$dropStatus);
			}
		}
	}
}
?>